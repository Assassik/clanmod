from messenger.proto.bw.ClanListener import ClanListener
from constants import AUTH_REALM
from helpers import getClientLanguage
from gui import SystemMessages
from gui.shared.ItemsCache import g_itemsCache
from notification.NotificationListView import NotificationListView
from datetime import datetime
from Account import Account
from traceback import print_exc

import BigWorld
import ResMgr
import game
import sys
import debug_utils

import string
import urllib2
import httplib
import socket
import json
import os
import re
import time
import threading


__author__ = 'BONNe1704'
__version__ = '0.9.10.13'

defConfig = {
    'messages': {
        'config': {
            'load_error': '<font face=\'$FieldFont\' size=\'13\'>Config not found. Use default settings!</font>',
            'server_error': '<font face=\'$FieldFont\' size=\'13\'>Server not supported.</font>',
            'update': '<font face=\'$FieldFont\' size=\'13\'><b>Update available:</b> </font>',
            'reload': '<font face=\'$FieldFont\' size=\'13\'><b>Configuration and language files reloaded!</b> </font>'
        },
        'language': {
            'error': '<font face=\'$FieldFont\' size=\'13\'>Language missing in clanMod.lang file. Will use en!</font>'
        },
        'clanmod': {
            'online': '<font face=\'$FieldFont\' size=\'13\' color=\'#BFE9FF\'><b>Online: {{online}} / {{members}}</b></font>',
            'button': 'Clan Mod',
            'print_done': '<font face=\'$FieldFont\' size=\'13\'>Online members saved to file: </font>\n',
            'print_error': '<font face=\'$FieldFont\' size=\'13\'>Error on saving members into file. Check python.log!</font>',
            'not_in_clan': '<font face=\'$FieldFont\' size=\'13\'>You are not in clan. clanMod is switched off!</font>'
        },
        'clanwars': {
            'message': {
                'title': '<font face=\'$FieldFont\' size=\'13\' color=\'#BFE9FF\'><b>ClanWar list:</b></font>\n',
                'message': [
                    '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\'><b>Province:<tab><tab>{{landing}} ( {{income}}</b><img src=\'img://gui/maps/icons/library/GoldIcon-2.png\' width=\'16\' height=\'16\' align=\'baseline\' vspace=\'-3\'><b>)</b></font></textformat>',
                    '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\' color=\'#BFE9FF\'><b>Time:<tab><tab>{{time}}</b></font></textformat>',
                    '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\'>Map:<tab><tab>{{map}}</font></textformat>',
                    '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\' color=\'{{color}}\'>Owner:<tab><tab><img src=\'{{icon}}\' width=\'16\' height=\'16\' align=\'baseline\' vspace=\'-3\'>{{owner}}</font></textformat>',
                    '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\'>Type:<tab><tab>{{type}}</font></textformat>\n'
                ],
                'spacer': '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\' color=\'#BFE9FF\'><b>------------------------------</b></font></textformat>\n',
                'attack': 'Attacking',
                'defense': 'Defending',
                'approximately': '~',
                'new_clan_war': '<font face=\'$FieldFont\' size=\'13\'>New Clan War battle!</font>',
                'none_battles': 'No ClanWars<br>'
            },
            'net_error': '<font face=\'$FieldFont\' size=\'13\'>Error on getting ClanWar data. Possible api or network problems.</font>',
            'save_error': '<font face=\'$FieldFont\' size=\'13\'>Error while saving ClanWar data to file. Check python.log!</font>',
            'data_saved': '<font face=\'$FieldFont\' size=\'13\'>ClanWars data updated!</font>',
            'button': 'Battles'
        },
        'stronghold': {
            'message': {
                'title': '<font face=\'$FieldFont\' size=\'13\' color=\'#BFE9FF\'><b>Stronghold Battle list:</b></font>\n',
                'message': [
                    '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\' color=\'{{color}}\'>Opponent:<tab><tab><img src=\'{{icon}}\' width=\'16\' height=\'16\' align=\'baseline\' vspace=\'-3\'>{{opponent}}</font></textformat>',
                    '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\' color=\'#BFE9FF\'><b>Time:<tab><tab>{{planned_date}}</b></font></textformat>',
                    '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\'>Type:<tab><tab>{{type}}</font></textformat>\n'
                ],
                'spacer': '<textformat tabstops=\'[10,40]\'><font face=\'$FieldFont\' size=\'13\' color=\'#BFE9FF\'><b>------------------------------</b></font></textformat>\n',
                'attack': 'Attacking',
                'defense': 'Defending',
                'approximately': '~',
                'date_format': '%Y.%m.%d %H:%M',
                'new_battle': '<font face=\'$FieldFont\' size=\'13\'>New Stronghold battle!</font>',
                'none_battles': 'No Stronghold battles<br>'
            },
            'net_error': '<font face=\'$FieldFont\' size=\'13\'>Error on getting Stronghold data. Possible api or network problems.</font>',
            'save_error': '<font face=\'$FieldFont\' size=\'13\'>Error while saving Stronghold data to file. Check python.log!</font>',
            'data_saved': '<font face=\'$FieldFont\' size=\'13\'>Stronghold data updated!</font>'
        },
        'provinces': {
            'no_file': 'File <b>clanMod.prov</b> not found!',
            'no_province': 'Province not found in file. Please delete <b>clanMod.prov</b>',
            'load_error': 'Error on getting data from WG API. Net or Api problems.',
            'updated': 'Province file created and data updated!',
            'save_error': 'Error on saving province file. Check python.log!',
            'owner_get_error': 'Error on getting province ovner data.'
        }
    },
    'icon': 'gui/maps/icons/tankmen/crew/retrainCrew.png',
    'save_button': 'F12',
    'update_button': 'F11',
    'reload_button': 'F10',
    'list_separator': ',',
    'save_path': 'online/',
    'update': {
        'notify': 'all',
        'show_changelog': True
    },
    'member_export': {
        'enabled': False,
        'show_messages': True
    },
    'clan_wars': {
        'enabled': True,
        'show_messages': True,
        'save_to_xvm': False
    },
    'stronghold': {
        'enabled': True,
        'show_messages': True,
        'save_to_xvm': False
    },
    'updating': {
        'enabled': True,
        'frequency': 300
    },
    'force_lang': None,
    'first_message': 'ClanMod',
    'debug': True,
    'subversion': '0'
}
notification = True
# Backup values for mod functionality
handleKeyBackup = game.handleKeyEvent
accountOnBecomePlayerBackup = Account.onBecomePlayer
notificationListBackup = NotificationListView._populate
notificationClickBackup = NotificationListView.onClickAction


# Logger class log mod messages if debug is turn on
class Logger(object):
    file = None
    debug = False

    def __init__(self):
        if not os.path.exists('logs/clanMod.log'):
            open('logs/clanMod.log', 'w').close()
        self.file = open('logs/clanMod.log', 'a')
        message = datetime.now().strftime("%d/%m/%Y %H:%M") + " [clanMod] Mod started." + "\n"
        message += datetime.now().strftime("%d/%m/%Y %H:%M") + " [clanMod] Author: " + __author__ + "\n"
        message += datetime.now().strftime("%d/%m/%Y %H:%M") + " [clanMod] Version: " + __version__ + "\n"
        self.file.write(message)

    def turn_on_debug(self):
        self.debug = True

    def print_message(self, message, source="[CLANMOD]"):
        if self.debug:
            print_message = datetime.now().strftime("%d/%m/%Y %H:%M") + " " + source + " " + str(message) + "\n"
            self.file.write(print_message)

    def print_exception(self):
        self.file.write(debug_utils._makeMsgHeader(sys._getframe(1)))
        print_exc(file=self.file)

    def __del__(self):
        self.file.write("\n" + ">------------------------------------------<" + "\n")
        self.file.close()

# Init logger class
logger = Logger()


# Method removes comments from file
def strip_comments(text):
    regex = r'( |\t)*//.*$'
    regex_inline = r'(:?(?: |\t)*([A-Za-z\d\.{}]*)|(\".*\"),?)(?: |\t)*(//.*$)'
    lines = text.split('\n')
    excluded = []

    for index in xrange(len(lines)):
        if re.search(regex, lines[index]):
            if re.search(r'^' + regex, lines[index]):
                excluded.append(lines[index])
            elif re.search(regex_inline,
                           lines[index]):
                lines[index] = re.sub(regex_inline,
                                      r'\1', lines[index])

    for line in excluded:
        lines.remove(line)

    message = '\n'.join(lines)
    return message


# Method sets server and its api
def get_server():
    server = 'null'
    api = 'null'

    if AUTH_REALM == 'RU':
        server = 'ru'
        api = '61424f09e17fbc381df29bdba46b48e3'
    elif AUTH_REALM == 'EU':
        server = 'eu'
        api = '75fbf689a19d9e83c692f92375bd446a'
    elif AUTH_REALM == 'NA':
        server = 'com'
        api = '2ebc4561ec55d12e7efd2bcb355bcb13'
    elif AUTH_REALM == 'ASIA':
        server = 'asia'
        api = 'demo'
    elif AUTH_REALM == 'KR':
        server = 'kr'
        api = 'demo'

    logger.print_message({'server': server, 'api': api}, "[SERVER]")
    return {'server': server, 'api': api}


# Method choose user language file, or use def_config
def get_lang(force=None):
    if force is None:
        check_lang = getClientLanguage()
    else:
        check_lang = force

    if os.path.isfile('res_mods/configs/clanMod/language/' + check_lang + '.lang'):
        try:
            check_file = open('res_mods/configs/clanMod/language/' + check_lang + '.lang')
            data = json.load(check_file)
            check_file.close()
            language = data
        except Exception as error:
            language = defConfig['messages']
            logger.print_exception()
            msg = defConfig['messages']['language']['error']
            SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)
    else:
        if get_server()['server'] == 'ru':
            check_lang = 'ru'
        else:
            check_lang = 'en'

        if os.path.isfile('res_mods/configs/clanMod/language/' + check_lang + '.lang'):
            try:
                check_file = open('res_mods/configs/clanMod/language/' + check_lang + '.lang')
                data = json.load(check_file)
                check_file.close()
                language = data
            except Exception as error:
                language = defConfig['messages']
                logger.print_exception()
                msg = defConfig['messages']['language']['error']
                SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)
        else:
            language = defConfig['messages']

    logger.print_message(language, "[LANGUAGE]")
    return language


# Init lang variable for usage
lang = get_lang()


# Load config or use default
def get_config():
    data = defConfig

    try:
        if os.path.isfile('res_mods/configs/clanMod/clanMod.cfg'):
            json_data = open('res_mods/configs/clanMod/clanMod.cfg')
            striped = strip_comments(json_data.read())
            data = json.loads(striped)
            json_data.close()
        else:
            logger.print_message("Missing config file")
            msg = defConfig['messages']['config']['load_error']
            SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)
    except Exception as error:
        logger.print_exception()
        msg = defConfig['messages']['config']['load_error']
        SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)

    logger.print_message(data, "[CONFIG]")

    if data['debug']:
        logger.turn_on_debug()

    if data['force_lang'] is not None:
        global lang
        lang = get_lang(data['force_lang'])

    return data


# Init config variable for usage
config = get_config()


# TimerThread class calls run_timer method that will run in other tread.
class TimerThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        logger.print_message("Thread is initialized", "[THREAD]")
        self.is_running = False

    def isRunning(self):
        return self.is_running

    def run(self):
        self.is_running = True
        logger.print_message("Thread Started", "[THREAD]")

        while config['updating']['enabled']:
            frequency = config['updating']['frequency']
            logger.print_message("Timer now check updating possibility every " + str(frequency), "[THREAD]")

            if timer() and clan.infoData['server'] != 'null':
                if config['clan_wars']['enabled']:
                    clan.update_clan_wars()
                if config['stronghold']['enabled']:
                    clan.update_stronghold_battles()
                if config['member_export']['enabled']:
                    clan.print_members()

            if frequency < 10:
                logger.print_message("< 10 sec frequency", "[THREAD]")
                msg = "ERROR_LESS_THAN_10"
                SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)
                frequency = 10

            time.sleep(frequency)

        logger.print_message("Timer stopped!", "[THREAD]")
        self.is_running = False
        threading.Thread.__init__(self)

timer_thread = TimerThread()
timer_thread.setDaemon(True)


# Method downloads ask_data with given **kwarg and export them as JSON
def download_data(ask_data, **kwarg):
    url = 'http://api.worldoftanks.' + kwarg['server']
    if ask_data == 'GetClanWars':
        url = url + '/wot/globalmap/clanbattles/?clan_id=' + str(kwarg['clan_id'])
    elif ask_data == 'GetProvinceData':
        url = url + '/wot/globalwar/provinces/?front_id=' + str(kwarg.get('front_id'))
        url = url + '&province_id=' + str(kwarg.get('province_id'))
    elif ask_data == 'GetClanInfo':
        url += '/wgn/clans/info/?fields=name,tag,color,emblems'
        url = url + '&clan_id=' + str(kwarg['enemy'])
    elif ask_data == 'GetMapInfo':
        url += '/wot/globalmap/info/?state,last_turn_calculated_at,last_turn'
    elif ask_data == 'GetStrongholdBattles':
        url += '/wot/stronghold/plannedbattles/?clan_id=' + str(kwarg['clan_id'])

    url = url + '&application_id=' + kwarg['api']

    if kwarg['server'] == 'ru':
        url = url + '&language=' + kwarg.get('lang', 'ru')
    else:
        url = url + '&language=' + kwarg.get('lang', 'en')

    try:
        req = urllib2.Request(url)
        req.add_header('Accept', 'application/json')
        res = json.load(urllib2.urlopen(req, timeout=5))
    except httplib.BadStatusLine and urllib2.HTTPError as error:
        logger.print_exception()
        SystemMessages.pushMessage(lang['clanwars']['net_error'])
        return None
    except socket.timeout as error:
        logger.print_exception()
        SystemMessages.pushMessage(lang['clanwars']['net_error'])
        return None
    except Exception as error:
        logger.print_exception()
        return None

    logger.print_message(res, "[DOWNLOAD]")

    if res['status'] != 'ok':
        if res['error']['field'] == 'language':
            kwarg['lang'] = ''
            clan.infoData['lang'] = ''
            return_data = download_data(ask_data, **kwarg)
            return return_data
        elif res['error']['field'] == 'map_id':
            return res
        else:
            return None

    return res


# Save image to file
def download_icon(url, clan):
    req = urllib2.urlopen(url).read()
    output = 'res_mods/configs/clanMod/icons'

    if config['clan_wars']['save_to_xvm']:
        output = 'res_mods/xvm/res/clanicons/' + AUTH_REALM + '/clan'

    if not os.path.exists(output):
        os.makedirs(output)

    image = open(output + '/' + clan + '.png', 'wb')
    image.write(req)
    image.close()


# Method gets update and returns Update message
def get_update():
    version = False
    subversion = False
    boolean = False

    try:
        url = 'http://pastebin.com/raw.php?i=7hwMCa1t'
        req = urllib2.Request(url)
        req.add_header('Accept', 'application/json')
        res = urllib2.urlopen(req)
        res = json.load(res)

        logger.print_message(res, "[UPDATE]")

        if config['update']['notify'] == 'all':
            boolean = True

        if res['version'] != __version__:
            x, y, z, w = res['version'].split('.')
            mod_x, mod_y, mod_z, mod_w = __version__.split('.')

            if x <= mod_x and y <= mod_y and z <= mod_z and w <= mod_w:
                version = False
            else:
                version = True

        if res['subversion'] > config['subversion']:
            subversion = True

        if (version or subversion) and boolean:
            message = lang['config']['update'] + '<font face=\'$FieldFont\' size=\'13\' color=\'#BFE9FF\'><b>'
            message += res['version']

            if subversion and boolean:
                message += '-' + res['subversion']

            message += '</b></font>\n'

            if config['update']['show_changelog']:
                temp = '\n<textformat tabstops=\'[10,40]\'>'
                temp += '<img src=\'img://gui/maps/icons/buttons/tab_sort_button/ok.png\' width=\'11\' height=\'11\' '
                temp += 'align=\'baseline\' vspace=\'-2\'><font face=\'$FieldFont\' size=\'11\'>'

                if version:
                    if res['changelog'].get(getClientLanguage()):
                        temp += '</font></textformat>\n<textformat tabstops=\'[10,40]\'>'
                        temp += '<img src=\'img://gui/maps/icons/buttons/tab_sort_button/ok.png\' width=\'11\' '
                        temp += 'height=\'11\' align=\'baseline\' vspace=\'-2\'>'
                        temp += '<font face=\'$FieldFont\' size=\'11\'>'.join(res['changelog'][getClientLanguage()])
                    else:
                        if get_server()['server'] == 'ru':
                            temp += '</font></textformat>\n<textformat tabstops=\'[10,40]\'>'
                            temp += '<img src=\'img://gui/maps/icons/buttons/tab_sort_button/ok.png\' width=\'11\' '
                            temp += 'height=\'11\' align=\'baseline\' vspace=\'-2\'>'
                            temp += '<font face=\'$FieldFont\' size=\'11\'>'.join(res['changelog']['ru'])
                        else:
                            temp += '</font></textformat>\n<textformat tabstops=\'[10,40]\'>'
                            temp += '<img src=\'img://gui/maps/icons/buttons/tab_sort_button/ok.png\' width=\'11\' '
                            temp += 'height=\'11\' align=\'baseline\' vspace=\'-2\'>'
                            temp += '<font face=\'$FieldFont\' size=\'11\'>'.join(res['changelog']['en'])

                if subversion:
                    if res['subchanges'].get(getClientLanguage()):
                        temp += '</font></textformat>\n<textformat tabstops=\'[10,40]\'>'
                        temp += '<img src=\'img://gui/maps/icons/buttons/tab_sort_button/ok.png\' width=\'11\' '
                        temp += 'height=\'11\' align=\'baseline\' vspace=\'-2\'>'
                        temp += '<font face=\'$FieldFont\' size=\'11\'>'.join(res['subchanges'][getClientLanguage()])
                    else:
                        if get_server()['server'] == 'ru':
                            temp += '</font></textformat>\n<textformat tabstops=\'[10,40]\'>'
                            temp += '<img src=\'img://gui/maps/icons/buttons/tab_sort_button/ok.png\' width=\'11\' '
                            temp += 'height=\'11\' align=\'baseline\' vspace=\'-2\'>'
                            temp += '<font face=\'$FieldFont\' size=\'11\'>'.join(res['subchanges']['ru'])
                        else:
                            temp += '</font></textformat>\n<textformat tabstops=\'[10,40]\'>'
                            temp += '<img src=\'img://gui/maps/icons/buttons/tab_sort_button/ok.png\' width=\'11\' '
                            temp += 'height=\'11\' align=\'baseline\' vspace=\'-2\'>'
                            temp += '<font face=\'$FieldFont\' size=\'11\'>'.join(res['subchanges']['en'])

                temp += '</font></textformat>'
            else:
                temp = ''

            return message + temp
        return None
    except Exception as error:
        logger.print_exception()
        return None


# Method runs timer and returns true, if timer is ready, otherwise false
def timer():
    try:
        clan_name = clanListener.get_clan_name()

        if clan_name == '':
            boolean = False
        else:
            output = ResMgr.openSection('../configs/clanMod/clanMod.xml', True)
            if output.has_key(clan_name):
                if not output[clan_name].has_key('last_turn'):
                    output[clan_name].writeInt('last_turn', 0)
            else:
                output.write(clan_name, '')
                output[clan_name].writeInt('last_turn', 0)

            map_status = download_data('GetMapInfo', **get_server())

            if int(map_status['data']['last_turn']) != output[clan_name].readInt('last_turn'):
                logger.print_message(output[clan_name].readInt('last_turn'), "[TIMER] Previous Turn ID")
                logger.print_message(map_status['data']['last_turn'], "[TIMER] New Turn ID")

                output[clan_name].writeInt('last_turn', map_status['data']['last_turn'])
                output[clan_name].writeInt('last_turn_calculated_at', map_status['data']['last_turn_calculated_at'])
                output[clan_name].writeString('state', map_status['data']['state'])

                logger.print_message(map_status['data']['state'] +
                                     " " +
                                     str(map_status['data']['last_turn_calculated_at']),
                                     "[TIMER]")
                boolean = True
            else:
                logger.print_message(map_status['data']['last_turn'], "[TIMER] Turn ID")

                boolean = False

            output.save()

        return boolean
    except Exception as error:
        logger.print_exception()
        return False


# Class extends ClanListener, to get clan name and online members
class MyClanListener(ClanListener):
    clanMemberCount = 1
    onlineMemberCount = 1
    onlineMembers = []

    # Method returns clan Name
    def get_clan_name(self):
        return self.playerCtx.getClanAbbrev()

    # Method returns clan ID. Method can be use anywhere, but i want to use it there :D
    @staticmethod
    def get_clan_id():
        return g_itemsCache.items.stats.clanDBID

    # Method go through members list and online members put in other list
    def get_online_members(self):
        self.onlineMembers = []
        self.clanMemberCount = 1
        self.onlineMemberCount = 1

        members = self.usersStorage.getClanMembersIterator()

        for member in members:
            self.clanMemberCount += 1
            if member.isOnline():
                self.onlineMemberCount += 1
                self.onlineMembers.append(member)


# Add reference to MyClanListener class
clanListener = MyClanListener()


# Method returns if user has clan or not
def has_clan():
    if clanListener.get_clan_name() is None:
        return False

    return True


# Main class for representing all data
class ClanMod(object):
    state = config['first_message']
    infoData = {}

    def __init__(self):
        self.infoData = get_server()
        self.infoData['lang'] = getClientLanguage()
        self.infoData['clan_id'] = clanListener.get_clan_id()
        # self.infoData['server'] = 'ru'
        # self.infoData['api'] = '61424f09e17fbc381df29bdba46b48e3'
        logger.print_message(self.infoData, "[ClanMod]")

    # Method change message state form standard message to clan wars message
    def change_state(self):
        if self.state == 'ClanMod':
            self.state = 'ClanWars'
        else:
            self.state = 'ClanMod'

    # Method creates message which will be represented in notification list
    def create_message(self):
        if self.state == 'ClanMod':
            msg = self.standard_message()
        else:
            msg = self.clan_wars_message() + self.strongholds_message()

        message = {
            'typeID': 1,
            'message': {
                'bgIcon': '',
                'defaultIcon': '',
                'savedID': 0,
                'timestamp': -1,
                'filters': [],
                'buttonsLayout': [],
                'message': msg,
                'type': 'Information',
                'icon': '../../' + config['icon']},
            'hidingAnimationSpeed': 2000.0,
            'notify': True,
            'lifeTime': 6000.0,
            'entityID': 90000,
            'auxData': ['GameGreeting']}

        if self.state == 'ClanMod':
            if config['clan_wars']['enabled']:
                message['message']['buttonsLayout'].append({
                    'action': 'ClanWars',
                    'type': 'submit',
                    'label': lang['clanwars']['button']})
        elif self.state == 'ClanWars':
            message['message']['buttonsLayout'].append({
                'action': 'ClanMod',
                'type': 'submit',
                'label': lang['clanmod']['button']})
        return message

    # Method create and returns Online member message
    @staticmethod
    def standard_message():
        clanListener.get_online_members()
        message = lang['clanmod']['online']
        message = string.replace(message, '<br>', '\n')
        message = string.replace(message, '{{online}}', str(clanListener.onlineMemberCount))
        message = string.replace(message, '{{members}}', str(clanListener.clanMemberCount))
        return message

    # Method create and returns ClanWars message
    def clan_wars_message(self):
        cache = ResMgr.openSection('../configs/clanMod/clanMod.xml', True)
        clan_name = clanListener.get_clan_name()

        if not cache or not cache.has_key(clan_name) or not cache[clan_name].has_key('cw_count'):
            return lang['clanwars']['message']['none_battles']

        count = cache[clan_name].readInt('cw_count')
        if count == 0:
            return lang['clanwars']['message']['none_battles']

        message = lang['clanwars']['message']['title']

        if message != '':
            message += '\n'

        while count > 0:
            front_name = cache[clan_name]['global_map'][str(count)].readString('front_name')
            province_name = cache[clan_name]['global_map'][str(count)].readString('province_name')
            arena_name = cache[clan_name]['global_map'][str(count)].readString('arena_name')

            status = cache[clan_name]['global_map'][str(count)].readString('status')
            time = cache[clan_name]['global_map'][str(count)].readString('time')
            battle_type = cache[clan_name]['global_map'][str(count)].readString('type')
            pre_time = cache[clan_name]['global_map'][str(count)].readString('pre_time')

            enemy = cache[clan_name]['global_map'][str(count)].readString('enemy')
            enemy_color = cache[clan_name]['global_map'][str(count)].readString('enemy_color')
            enemy_icon = cache[clan_name]['global_map'][str(count)].readString('enemy_icon')

            icon = 'img://../xvm/res/clanicons/' + AUTH_REALM + '/clan/' + enemy + '.png'

            if not os.path.isfile('res_mods/xvm/res/clanicons/' + AUTH_REALM + '/clan/' + enemy + '.png'):
                if not os.path.isfile('res_mods/configs/clanMod/icons/' + enemy + '.png'):
                    download_icon(enemy_icon, enemy)

                if not config['clan_wars']['save_to_xvm']:
                    icon = 'img://../configs/clanMod/icons/' + enemy + '.png'

            # time fix to get timezone offset
            try:
                utc_offset = datetime.now() - datetime.utcnow()
                if utc_offset.days == -1:
                    utc_offset = datetime.utcnow() - datetime.now()
                    pre_time = datetime.strptime(pre_time, "%H:%M") - utc_offset
                else:
                    pre_time = datetime.strptime(pre_time, "%H:%M") + utc_offset
            except Exception as error:
                logger.print_exception()

            time = datetime.fromtimestamp(time).strftime("%H:%M")

            template = '\n'.join(lang['clanwars']['message']['message'])

            template = string.replace(template, '<br>', '\n')
            template = string.replace(template, '{{map}}', arena_name)
            template = string.replace(template, '{{time}}', time)
            template = string.replace(template, '{{landing}}', province_name)

            template = string.replace(template, '{{type}}', lang['clanwars']['message'][battle_type])
            template = string.replace(template, '{{icon}}', icon)
            template = string.replace(template, '{{color}}', enemy_color)
            template = string.replace(template, '{{opponent}}', enemy)

            message += template

            count -= 1
            if count != 0:
                if lang['clanwars']['message']['spacer'] != '':
                    message += lang['clanwars']['message']['spacer']
                else:
                    message += '\n'
        return message

    # Method create and returns STRONGHOLDS message
    def strongholds_message(self):
        cache = ResMgr.openSection('../configs/clanMod/clanMod.xml', True)
        clan_name = clanListener.get_clan_name()

        if not cache or not cache.has_key(clan_name) or not cache[clan_name].has_key('sh_count'):
            return lang['stronghold']['message']['none_battles']

        count = cache[clan_name].readInt('sh_count')
        if count == 0:
            return lang['stronghold']['message']['none_battles']

        message = lang['stronghold']['message']['title']

        if message != '':
            message += '\n'

        while count > 0:

            planned_date = cache[clan_name]['stronghold'][str(count)].readInt('battle_planned_date')
            creation_date = cache[clan_name]['stronghold'][str(count)].readInt('battle_creation_date')
            attack_direction = cache[clan_name]['stronghold'][str(count)].readString('attack_direction')
            defense_direction = cache[clan_name]['stronghold'][str(count)].readString('defense_direction')
            type = cache[clan_name]['stronghold'][str(count)].readString('type')

            enemy = cache[clan_name]['stronghold'][str(count)].readString('enemy')
            enemy_color = cache[clan_name]['stronghold'][str(count)].readString('enemy_color')
            enemy_icon = cache[clan_name]['stronghold'][str(count)].readString('enemy_icon')

            icon = 'img://../xvm/res/clanicons/' + AUTH_REALM + '/clan/' + enemy + '.png'

            if not os.path.isfile('res_mods/xvm/res/clanicons/' + AUTH_REALM + '/clan/' + enemy + '.png'):
                if not os.path.isfile('res_mods/configs/clanMod/icons/' + enemy + '.png'):
                    download_icon(enemy_icon, enemy)

                if not config['clan_wars']['save_to_xvm']:
                    icon = 'img://../configs/clanMod/icons/' + enemy + '.png'

            template = '\n'.join(lang['stronghold']['message']['message'])

            template = string.replace(template, '<br>', '\n')
            template = string.replace(template, '{{planned_date}}', datetime.fromtimestamp(planned_date).strftime(lang['stronghold']['message']['date_format']))
            template = string.replace(template, '{{creation_date}}', datetime.fromtimestamp(creation_date).strftime(lang['stronghold']['message']['date_format']))
            template = string.replace(template, '{{defense_direction}}', defense_direction)
            template = string.replace(template, '{{attack_direction}}', attack_direction)

            template = string.replace(template, '{{type}}', lang['stronghold']['message'][type])
            template = string.replace(template, '{{icon}}', icon)
            template = string.replace(template, '{{color}}', enemy_color)
            template = string.replace(template, '{{opponent}}', enemy)

            message += template

            count -= 1
            if count != 0:
                if lang['stronghold']['message']['spacer'] != '':
                    message += lang['stronghold']['message']['spacer']
                else:
                    message += '\n'
        return message

    # Method update clan wars cache file
    def update_clan_wars(self):
        output = ResMgr.openSection('../configs/clanMod/clanMod.xml', True)
        res = download_data('GetClanWars', **self.infoData)

        if res is None:
            return

        if res['status'] != 'ok':
            msg = lang['clanwars']['net_error']
            SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)
            return

        try:
            clan_name = str(clanListener.get_clan_name())

            if clan_name is None:
                return

            if output.has_key(clan_name):
                new_clan_war = output[clan_name].readInt('cw_count')

                if output[clan_name].has_key('global_map'):
                    output[clan_name].deleteSection('global_map')
            else:
                output.write(clan_name, '')

            short_data = []

            if res.has_key('data'):
                short_data = res['data']

                if short_data is None:
                    return

            count = len(short_data)

            if count > new_clan_war:
                message = lang['clanwars']['message']['new_clan_war']
                SystemMessages.pushMessage(message, type=SystemMessages.SM_TYPE.Warning)

            output[clan_name].writeInt('cw_count', count)
            if count != 0:
                while count > 0:
                    count -= 1

                    self.infoData['province_id'] = short_data[count]['province_id']
                    self.infoData['front_id'] = short_data[count]['front_id']

                    province_data = download_data('GetProvinceData', **self.infoData)['data'][0]
                    active_battles = province_data['active_battles']
                    battles_count = len(active_battles)
                    curr_index = 0
                    is_founded = False

                    while curr_index < battles_count and not is_founded:
                        clan_a = active_battles[str(curr_index)]['clan_a']['clan_id']
                        clan_b = active_battles[str(curr_index)]['clan_b']['clan_id']

                        if clan_a == self.infoData['clan_id'] or clan_b == self.infoData['clan_id']:
                            is_founded = True

                            if clan_a == self.infoData['clan_id']:
                                self.infoData['enemy'] = clan_b
                            else:
                                self.infoData['enemy'] = clan_a

                        curr_index += 1

                    battle_type = short_data[count]['type']
                    owner = province_data['data'][short_data[count]['province_id']]['owner_clan_id']

                    if owner == self.infoData['enemy'] and province_data['status'] != 'FINISHED':
                        battle_type = 'BATTLE_AGAINST_OWNER'

                    if self.infoData['enemy'] is not None:
                        enemy_attributes = download_data('GetClanInfo', **self.infoData)
                        if enemy_attributes is None:
                            return

                        enemy = enemy_attributes['data'][str(self.infoData['enemy'])]['tag']
                        enemy_color = enemy_attributes['data'][str(self.infoData['enemy'])]['color']
                        enemy_icon = enemy_attributes['data'][str(self.infoData['enemy'])]['emblems']['x64']['wot']

                    output[clan_name].write('global_map', '')
                    output[clan_name]['global_map'].write(str(count + 1), '')

                    shortcut = output[clan_name]['global_map'][str(count + 1)]

                    shortcut.writeString('front_id', short_data[count]['front_id'])
                    shortcut.writeString('front_name', short_data[count]['front_name'])
                    shortcut.writeString('province_id', short_data[count]['province_id'])
                    shortcut.writeString('province_name', short_data[count]['province_name'])
                    shortcut.writeString('type', battle_type)
                    shortcut.writeInt('time', short_data[count]['time'])

                    shortcut.writeString('status', province_data['status'])
                    shortcut.writeString('arena_id', province_data['status'])
                    shortcut.writeString('arena_name', province_data['status'])
                    shortcut.writeString('prime_time', province_data['status'])

                    shortcut.writeString('enemy', enemy)
                    shortcut.writeString('enemy_color', enemy_color)
                    shortcut.writeString('enemy_icon', enemy_icon)

            if config['clan_wars']['show_messages']:
                msg = lang['clanwars']['data_saved']
                SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Warning)
            output.save()
        except Exception as error:
            logger.print_exception()
            msg = lang['clanwars']['save_error']
            SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)

    # Method update stronghold cache file
    def update_stronghold_battles(self):
        output = ResMgr.openSection('../configs/clanMod/clanMod.xml', True)
        res = download_data('GetStrongholdBattles', **self.infoData)

        if res is None:
            return

        if res['status'] != 'ok':
            msg = lang['stronghold']['net_error']
            SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)
            return

        try:
            clan_name = str(clanListener.get_clan_name())
            if clan_name is None:
                return

            planed_battles = 0

            if output.has_key(clan_name):
                if output[clan_name].has_key('stronghold'):
                    planed_battles = self.get_completed_battle_count()
            else:
                output.write(clan_name, '')

            short_data = []

            if res.has_key('data') and res['data'] is not None:
                if res['data'].has_key(str(self.infoData['clan_id'])):
                    short_data = res['data'][str(self.infoData['clan_id'])]

                    if short_data is None:
                        return
                else:
                    logger.print_message("Clan not found", "[STRONGHOLD]")
                    return
            else:
                return

            count = len(short_data)

            if count > planed_battles:
                message = lang['stronghold']['message']['new_battle']
                SystemMessages.pushMessage(message, type=SystemMessages.SM_TYPE.Warning)

                if output[clan_name].has_key('stronghold'):
                    output[clan_name].deleteSection('stronghold')

                output[clan_name].writeInt('sh_count', count)
                if count != 0:
                    while count > 0:
                        count -= 1

                        if short_data[count]['battle_type'] == 'attack':
                            self.infoData['enemy'] = short_data[count]['defender_clan_id']
                        else:
                            self.infoData['enemy'] = short_data[count]['attacker_clan_id']

                        if self.infoData['enemy'] is not None:
                            enemy_attributes = download_data('GetClanInfo', **self.infoData)
                            if enemy_attributes is None:
                                return

                            enemy = enemy_attributes['data'][str(self.infoData['enemy'])]['tag']
                            enemy_color = enemy_attributes['data'][str(self.infoData['enemy'])]['color']
                            enemy_icon = enemy_attributes['data'][str(self.infoData['enemy'])]['emblems']['x64']['wot']
                        else:
                            enemy = '[NPC]'
                            enemy_color = 'gray'
                            enemy_icon = ''

                        output[clan_name].write('stronghold', '')
                        output[clan_name]['stronghold'].write(str(count + 1), '')

                        shortcut = output[clan_name]['stronghold'][str(count + 1)]

                        shortcut.writeInt('battle_planned_date', short_data[count]['battle_planned_date'])
                        shortcut.writeInt('battle_creation_date', short_data[count]['battle_creation_date'])
                        shortcut.writeString('attack_direction', short_data[count]['attack_direction'])
                        shortcut.writeString('defense_direction', short_data[count]['defense_direction'])
                        shortcut.writeString('type', short_data[count]['battle_type'])

                        shortcut.writeString('enemy', enemy)
                        shortcut.writeString('enemy_color', enemy_color)
                        shortcut.writeString('enemy_icon', enemy_icon)

                if config['stronghold']['show_messages']:
                    msg = lang['stronghold']['data_saved']
                    SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Warning)

            output.save()
        except Exception as error:
            logger.print_exception()
            msg = lang['stronghold']['save_error']
            SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)

    # Method prints all members to file
    @staticmethod
    def print_members():
        clanListener.get_online_members()
        members = clanListener.onlineMembers

        message = ''

        for member in range(len(members)):
            message += members[member].getName()
            message += config['list_separator']
        message += BigWorld.player().name

        try:
            output = 'res_mods/configs/clanMod/' + config['save_path']

            if not os.path.exists(output):
                os.makedirs(output)

            output += datetime.now().strftime('%Y%m%d%H%M') + '.txt'

            root = open(output, "w")
            root.write(message)
            root.close()
            if config['member_export']['show_messages']:
                msg = lang['clanmod']['print_done'] + "\n" + "\t" + config['save_path']
                msg += datetime.now().strftime('%Y%m%d%H%M') + '.txt'
                SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Warning)

            logger.print_message(message, "[PRINTING]")
        except Exception as error:
            logger.print_exception()
            message = lang['clanmod']['print_error']
            SystemMessages.pushMessage(message, type=SystemMessages.SM_TYPE.Error)

    # Method counts battles, that still planed.
    @staticmethod
    def get_completed_battle_count():
        not_completed_battles = 0
        output = ResMgr.openSection('../configs/clanMod/clanMod.xml', True)

        try:
            clan_name = str(clanListener.get_clan_name())

            battle_count = output[clan_name].readInt('sh_count')

            if battle_count != 0:
                while battle_count > 0:
                    logger.print_message(battle_count, "STRONGHOLD")

                    if datetime.utcfromtimestamp(output[clan_name]['stronghold'][str(battle_count)].readInt('battle_planned_date')) > datetime.utcnow():
                        not_completed_battles += 1
                    else:
                        output[clan_name]['stronghold'].deleteSection(str(battle_count))

                    battle_count -= 1

                new_count = len(output[clan_name]['stronghold'])

                output[clan_name].writeInt('sh_count', new_count)
                output.save()
        except Exception as error:
            logger.print_exception()
            msg = lang['stronghold']['save_error']
            SystemMessages.pushMessage(msg, type=SystemMessages.SM_TYPE.Error)

        return not_completed_battles

# Iterate clan mod
clan = ClanMod()


# Method rewrite key events to add new key events for mod
def hook_key_event(event):
    global config, lang
    try:
        save = BigWorld.stringToKey(config['save_button'])
        update = BigWorld.stringToKey(config['update_button'])
        reset = BigWorld.stringToKey(config['reload_button'])

        if not event.isRepeatedEvent() and event.isKeyDown() and event.key == save:
            clan.print_members()
        elif not event.isRepeatedEvent() and event.isKeyDown() and event.key == update:
            clan.update_clan_wars()
            clan.update_stronghold_battles()
        elif not event.isRepeatedEvent() and event.isKeyDown() and event.key == reset:
            config = get_config()
            lang = get_lang(config['force_lang'])
            message = lang['config']['reload']

            if config['updating']['enabled'] and not timer_thread.isRunning():
                timer_thread.start()

            SystemMessages.pushMessage(message, type=SystemMessages.SM_TYPE.Information)
        return False
    except Exception as error:
        logger.print_exception()
    finally:
        return handleKeyBackup(event)


# Method rewrite onBecomePlayer function, to load clan mod update messages
def new_on_become_player(self):
    global notification
    notification = False
    # Prints update if it is needed
    if (config['update']['notify'] == 'all' or config['update']['notify'] == 'important') and notification:
        notification = False
        message = get_update()
        if message is not None:
            SystemMessages.pushMessage(message, type=SystemMessages.SM_TYPE.GameGreeting)

    global clanListener
    clanListener = MyClanListener()

    if has_clan():
        try:
            if clan.infoData['clan_id'] == 0:
                clan.__init__()
            if not config['updating']['enabled'] and timer():
                if config['clan_wars']['enabled']:
                    clan.update_clan_wars()
                if config['member_export']['enabled']:
                    clan.print_members()
                if config['stronghold']['enabled']:
                    clan.update_stronghold_battles()
        except Exception as error:
            logger.print_exception()

    accountOnBecomePlayerBackup(self)


# Method rewrite notification list by adding new message from mod
def new_notification_list(self):
    notificationListBackup(self)
    try:
        if has_clan():
            if clan.infoData['clan_id'] == 0:
                clan.__init__()

            if not config['updating']['enabled'] and timer() and clan.infoData['server'] != 'null':
                if config['clan_wars']['enabled']:
                    clan.update_clan_wars()
                if config['member_export']['enabled']:
                    clan.print_members()
                if config['stronghold']['enabled']:
                    clan.update_stronghold_battles()

            self.as_appendMessageS(clan.create_message())

    except Exception as error:
        logger.print_exception()


# Method rewrite notification list button so it forks for this mod
def new_notification_click(self, typeid, entity, action):
    if action == 'ClanMod':
        clan.change_state()
    elif action == 'ClanWars':
        clan.change_state()
    else:
        notificationClickBackup(self, typeid, entity, action)


# Set up new variables
Account.onBecomePlayer = new_on_become_player
game.handleKeyEvent = hook_key_event
NotificationListView._populate = new_notification_list
NotificationListView.onClickAction = new_notification_click

# Region not supported message.
if clan.infoData['server'] == 'null':
    SystemMessages.pushMessage("Server not supported. If you think this is a mistake, report to mod author.")

# Create new Thread, that will update clanwars every X minute.
timer_thread.start()