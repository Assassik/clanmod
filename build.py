from py_compile import compile
import shutil
import os
import zipfile
# import pkg_resources

__version__ = '0.9.10'

def compiles():
    compile('res_mods/'+__version__+'/scripts/client/CameraNode.py')
    compile('res_mods/'+__version__+'/scripts/client/mods/__init__.py')
    compile('res_mods/'+__version__+'/scripts/client/mods/zClanMod.py')

source = os.getcwd() + os.sep + 'res_mods'
destination1 = os.getcwd() + os.sep + 'build' + os.sep + 'res_mods'
destination2 = 'D:' + os.sep + 'Games' + os.sep + 'World_of_Tanks' + os.sep + 'res_mods' + os.sep
destination3 = destination2 + 'configs' + os.sep + 'clanMod'
destination2 += __version__ + os.sep + 'scripts' + os.sep + 'client' + os.sep + 'mods'

def remove():
    try:
        if os.path.exists(destination1):
            shutil.rmtree(destination1)
            print(destination1)

    except Exception as error:
        print error

def create():
    shutil.copytree(source, destination1, ignore=shutil.ignore_patterns('*.py'))

def moveToFolder():
    folder1 = destination1 + os.sep + __version__ + os.sep + 'scripts' + os.sep + 'client' + os.sep + 'mods' + os.sep
    folder1 += 'zClanMod.pyc'
    folder2 = destination2 + os.sep + 'zClanMod.pyc'
    shutil.copy(folder1, folder2)

    folder3 = destination1 + os.sep + 'configs' + os.sep + 'clanMod' + os.sep + 'clanMod.cfg'
    folder4 = destination3 + os.sep + 'clanMod.cfg'
    shutil.copy(folder3, folder4)

    folder5 = destination1 + os.sep + 'configs' + os.sep + 'clanMod' + os.sep + 'language' + os.sep + 'lv.lang'
    folder6 = destination3 + os.sep + 'language' + os.sep + 'lv.lang'
    shutil.copy(folder5, folder6)

    folder5 = destination1 + os.sep + 'configs' + os.sep + 'clanMod' + os.sep + 'language' + os.sep + 'en.lang'
    folder6 = destination3 + os.sep + 'language' + os.sep + 'en.lang'
    shutil.copy(folder5, folder6)

def createZip():
    import re
    folder1 = 'res_mods/'+__version__+'/scripts/client/mods/zClanMod.py'
    main_py = open(folder1).read()
    metadata = dict(re.findall("__([a-z]+)__ = '([^']+)'", main_py))
    version = metadata['version']

    zf = zipfile.ZipFile("output/" + version + ".zip", "w")
    abs_src = os.path.abspath(destination1 + os.sep + '..' + os.sep)
    for dirname, subdirs, files in os.walk(destination1 + os.sep + '..' + os.sep):
        for filename in files:
            absname = os.path.abspath(os.path.join(dirname, filename))
            arcname = absname[len(abs_src) + 1:]
            print 'zipping %s as %s' % (os.path.join(dirname, filename),
                                        arcname)
            zf.write(absname, arcname)
    zf.close()


create()
compiles()
moveToFolder()
createZip()
remove()
